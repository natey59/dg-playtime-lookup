<?php
	
	require_once('mysqli_connect.php');
	
	$query = "SELECT * FROM `BungeeOnlineTime` ORDER BY `OnlineTime` DESC LIMIT 10;";
	
	$response = @mysqli_query($dbc, $query);
	
	if ($response) {
	
		echo "<table border=\"1\" style=\"width:100%\">";
		
		echo "<tr> <td> <b>Player</b> </td> <td> <b>Minutes Played</b> </td> </tr>";
	
		while ($row = mysqli_fetch_array($response)) {
			$user = $row['Player'];
			$time = $row['OnlineTime'];
			
			echo "<tr> <td> " . $user . "</td> <td>" . $time . "</td> </tr>";
			
		}
		
		echo "</table>";
		
	} else {
		echo "MySQL Error";
	}	
	
?>